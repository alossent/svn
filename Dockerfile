FROM fedora:29
MAINTAINER Alvaro Gonzalez <agonzale@cern.ch>

COPY repos/ /etc/yum.repos.d/
COPY httpd/ /etc/httpd/conf.d/
COPY websvn-checkout/ /usr/share/websvn/

#
# Reinstalling glibc-common, so we can have all locales installed. Something necessary for some hooks
#
RUN yum install -y httpd mod_dav_svn mod_ldap subversion php php-geshi php-xml php-mbstring \
        php-pear php-geshi enscript rsync openssh-clients SVN-egroups svnmailer subversion-tools nss_wrapper \
        CERN-CA-certs python2-subversion && \
    yum -y reinstall glibc-common && \
    yum clean all && \
    mkdir /var/svn && \
    mkdir -p /run/httpd/ && chown -R root:root /run/httpd && chmod a+rw /run/httpd && \
    sed -i 's/Listen 80/# Listen 80\nListen 8000/' /etc/httpd/conf/httpd.conf && \
    # Logs to stdout and stderr
    sed -i 's#^\s*ErrorLog.*#ErrorLog /dev/stderr#' /etc/httpd/conf/httpd.conf && \
    sed -i 's#^\s*CustomLog.*#CustomLog /dev/stdout combined#' /etc/httpd/conf/httpd.conf && \
    # MPM, set the 'prefork' instead of 'event'. Otherwise php does not work
    sed -i.bak 's/^#LoadModule mpm_prefork_module/LoadModule mpm_prefork_module/' /etc/httpd/conf.modules.d/00-mpm.conf && \
    sed -i 's/^LoadModule mpm_event_module/#LoadModule mpm_event_module/' /etc/httpd/conf.modules.d/00-mpm.conf && \
    # Config.php, copy the default and add a single repository in /var/svn
    cp /usr/share/websvn/include/distconfig.php /usr/share/websvn/include/config.php && \
    echo "\$config->addRepository('repository', 'file:///var/svn/');" >>/usr/share/websvn/include/config.php && \
    echo "\$config->useAccessFile('/var/svn/conf/authz', 'repository');" >>/usr/share/websvn/include/config.php && \
    rm -rf /usr/share/httpd/noindex/*

ADD html/ /usr/share/httpd/noindex/
ADD bin/* /usr/local/bin/

EXPOSE 8000

USER 1001

ENTRYPOINT ["httpd", "-D", "FOREGROUND"]
