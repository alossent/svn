# Help

This document covers specific cases not covered in the main [README](./README.md) document.

## Using NSS_WRAPPER

Because the UID of the container is generated dynamically, it will not have an associated entry in /etc/passwd. This can cause problems for applications that expect to be able to look up their UID.

Check <https://docs.openshift.com/enterprise/3.1/creating_images/guidelines.html#use-uid> for guidelines. `NSS_WRAPPER` is already installed.

Here you can see a `NSS_WRAPPER` snippet example:

```bash
# Use nss_wrapper to add the current user to /etc/passwd
# and enable the use of tools like ssh
USER_ID=$(id -u)
GROUP_ID=$(id -g)
# Pointless if running as root
if [[ "${USER_ID}" != '0' ]]; then
   export NSS_WRAPPER_PASSWD=/tmp/nss_passwd
   export NSS_WRAPPER_GROUP=/tmp/nss_group
   cp /etc/passwd $NSS_WRAPPER_PASSWD
   cp /etc/group  $NSS_WRAPPER_GROUP

   if ! getent passwd "${USER_ID}" >/dev/null; then
      # we need an entry in passwd for current user. Make sure there is no conflict
      sed -e '/^forge:/d' -i $NSS_WRAPPER_PASSWD
      echo "forge:x:${USER_ID}:${GROUP_ID}:Rundeck instance:/var/django/home:/sbin/nologin" >> $NSS_WRAPPER_PASSWD
   fi
   export LD_PRELOAD=libnss_wrapper.so
fi
```

## Miscelanea

You can create random tokens using __/dev/urandom__. This example create 35 character long password:

```
base64 /dev/urandom | head -c 35

```