#!/bin/bash

set -e

# logs in into Openshift with server, token and namespace
OPENSHIFT_SERVER=$1
TOKEN=$2
NAMESPACE=$3
oc login "${OPENSHIFT_SERVER}" --token="${TOKEN}"
oc project "${NAMESPACE}"

