#!/bin/bash
#

egroups  >/dev/null
if [ $? -eq 127 ];
then
    echo "ERROR: egroups command not found, exiting" >&2
    exit 23
fi

GROUP_REG='^\s*\[groups\]'
BEGIN_REG='^\s*# BEGIN EGROUPS DEFINITIONS'
END_REG='^\s*# END EGROUPS DEFINITIONS'
NOGROUP_REG='^\s*\['
SEDREG='s/^[[:space:]]*\([A-Za-z\-]*\)[[:space:]]*=.*/\1/'
COMMENTREG='^[[:space:]]*#.*'

ingroupsec=false
ingroupdef=false

AUTHZ=$1

if [[ -z $AUTHZ ]];
then
    echo "Use: $0 <AUTHZFILE>" >&2
    exit 1
fi

if [[ ! -f $AUTHZ ]];
then
    echo "File $AUTHZ not found" >&2
    exit 0
fi

# Temporal new authz file
TMPAUTHZ=$(mktemp)
# Stores egroups errors
TMPF=$(mktemp)

# Every line
cat "$AUTHZ" | while read -r line;
do
    # If not in the groups definition, just print the line
    if [[ "$ingroupdef" == false || $line =~ $NOGROUP_REG || $line =~ $END_REG  || $line =~ $COMMENTREG ]];
    then
        echo "$line" >>"$TMPAUTHZ"
    else
        group=$(echo "$line" | sed -e "$SEDREG")

        if [ -z "$group" ];
        then
            continue
        fi

        users=$(egroups --linear -get "$group" 2>"$TMPF")
        return="$?"

        if [ $return -eq 0 ];
        then
            echo "$group = $users" >>"$TMPAUTHZ"
        else
            # Remove the comment of the line
            newline=$(echo $line | sed -e 's/\([^#]*\)#.*/\1/')
            echo -n "$newline # ($return) " >>"$TMPAUTHZ"
            # print the new error
            cat $TMPF >>"$TMPAUTHZ"
        fi
    fi

    if [[ $line =~ $GROUP_REG ]];
    then
        ingroupsec=true
    elif [[ $line =~ $BEGIN_REG  && $ingroupsec ]];
    then
        ingroupdef=true
    elif [[ $line =~ $END_REG || $line =~ $NOGROUP_REG ]];
    then
        ingroupdef=false
        ingroupsec=false
    fi
done

# Overwrite it only if it different
if ! diff "$TMPAUTHZ" "$AUTHZ" >/dev/null ;
then
    # Different"
    if [ "$AUTHZ" -nt "${AUTHZ}.bck" ];
    then
        # ${AUTHZ} is newer than ${AUTHZ}.bck
        cp "$AUTHZ" "${AUTHZ}.bck"
    fi

    cp "$TMPAUTHZ" "$AUTHZ"
    touch "${AUTHZ}.bck"

    rm "$TMPAUTHZ" "$TMPF"
fi

